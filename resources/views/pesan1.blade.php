<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="styles.css">
    <title>MDR RACING</title>
  </head>
  <body>

  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">TOKO AKSESORIS MDR RACING</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
      <div class="navbar-nav">
        <a class="nav-link active" aria-current="page" href="/">Home</a>
        <a class="nav-link" href="">DAFTAR AKSESORIS</a>
      </div>
    </div>
  </div>
</nav>

  <h2> TOKO AKSESORIS MDR RACING</h2>
    <div class="card-body">
        <table class="table table-bordered">
            <tr>
                <th>Nama Barang</th>
                <th>Harga Barang</th>
                <th>Jumlah Pesanan</th>
                <th>Tanggal</th>
                <th>pilihan</th>
            </tr>
            @foreach ($dtpesan as $item)
            <tr>
                <td>{{ $item->nama_barang}}</td>
                <td>{{ $item->harga_barang}}</td>
                <td>{{ $item->jumlah_pesanan}}</td>
                <td>{{ $item->tanggal}}</td>
                <td>
                    <a href="{{ url('edit', $item->id)}}"><button class="btn btn-info btn-sm">edit</button> </a>
                     <a href="{{ url('hapus', $item->id)}}"><button class="btn btn-info btn-sm">hapus</button></a>
                </td>
            </tr>
            @endforeach
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </table>
        <div class="card-tools">
            <a href="{{ route('tambahpesan')}}" class="btn btn-success">Tambah<i class="fa fa_plus-square"></i></a>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

   
  </body>
</html>