<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" type="text/javascript" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="gaya.css">
    <title>TOKO MDR RACING</title>
    
</head>
<body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
      
      
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link mr-4" aria-current="page" href="/" role="button">HOME</a>
          </li>
          <li class="nav-item">
            <a class="nav-link mr-4" href="saya" role="button">DAFTAR AKSESORIS</a>
          </li>
          <li class="nav-item">
            <a class="nav-link mr-4" href="{{ route('pesan')}}" role="button">PESANAN</a>
          </li>
        </ul>
      </div>
    </div>
</nav>

  
    
    <div class="container-fluid">
        <div class="row">
           <div class="col-md-2">
                <div class="card border-dark">
                 <img src="image/aksesorispelangi.jpg" class="card-img-top" alt="...">
                 <div class="card-body">
                     <h5 class="card-title font-weight-bold">Spidu Motor</h5>
                     <label class="card-text harga">Rp. 100.000</label><br>
                 </div>
                </div>
            </div>

            <div class="col-md-2">
                <div class="card border-dark">
                <img src="image/branbo.jpg" class="card-img-top" alt="...">
                  <div class="card-body">
                      <h5 class="card-title font-weight-bold">Brambo</h5>
                      <label class="card-text harga">Rp. 60.000</label><br>
                  </div>
                </div>
        </div>

        <div class="col-md-2">
             <div class="card border-dark">
            <img src="image/chainz.jpg" class="card-img-top" alt="...">
              <div class="card-body">
                  <h5 class="card-title font-weight-bold">Kanalpot Chainz</h5>
                  <label class="card-text harga">Rp. 200.000</label><br>
              </div>
             </div>
         </div>

        <div class="col-md-2">
             <div class="card border-dark">
            <img src="image/dscbreak.jpg" class="card-img-top" alt="...">
              <div class="card-body">
                  <h5 class="card-title font-weight-bold">DSC Break</h5>
                  <label class="card-text harga">Rp. 150.000</label><br>
              </div>
             </div>
        </div>
       <div class="col-md-2">
        <div class="card border-dark">
       <img src="image/jok.jpg" class="card-img-top" alt="...">
         <div class="card-body">
             <h5 class="card-title font-weight-bold">Jok Rx King</h5>
             <label class="card-text harga">Rp. 300.000</label><br>
         </div>
        </div>
      </div>
      <div class="col-md-2">
        <div class="card border-dark">
       <img src="image/kanalpot.jpg" class="card-img-top" alt="...">
         <div class="card-body">
             <h5 class="card-title font-weight-bold">Knalpot</h5>
             <label class="card-text harga">Rp. 70.000</label><br>
         </div>
        </div>
      </div>
      <div class="col-md-2">
        <div class="card border-dark">
       <img src="image/klaksontoatigasuara.jpg" class="card-img-top" alt="...">
         <div class="card-body">
             <h5 class="card-title font-weight-bold">Kalakson 3 Suara</h5>
             <label class="card-text harga">Rp. 40.000</label><br>
         </div>
        </div>
      </div>
      <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/knalpotmodif.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Knalpot Modif</h5>
         <label class="card-text harga">Rp. 200.000</label><br>
     </div>
    </div>
  </div>
      <div class="col-md-2">
        <div class="card border-dark">
       <img src="image/knalpottanpasuara.jpg" class="card-img-top" alt="...">
         <div class="card-body">
             <h5 class="card-title font-weight-bold">Knalpot 3 Suara</h5>
             <label class="card-text harga">Rp. 1.000.000</label><br>
         </div>
        </div>
      </div>
      <div class="col-md-2">
        <div class="card border-dark">
       <img src="image/koplinghandle.jpg" class="card-img-top" alt="...">
         <div class="card-body">
             <h5 class="card-title font-weight-bold">Kopling Handle</h5>
             <label class="card-text harga">Rp. 50.000</label><br>
         </div>
        </div>
      </div>
      <div class="col-md-2">
        <div class="card border-dark">
       <img src="image/masterrem.jpg" class="card-img-top" alt="...">
         <div class="card-body">
             <h5 class="card-title font-weight-bold">Master rem</h5>
             <label class="card-text harga">Rp. 50.000</label><br>
         </div>
        </div>
      </div>
   <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/pedal.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Pedal</h5>
         <label class="card-text harga">Rp. 25.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/remcakram.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Rem cakram</h5>
         <label class="card-text harga">Rp. 75.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/remrcb.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Rem RCB</h5>
         <label class="card-text harga">Rp. 150.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/sok.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Shok</h5>
         <label class="card-text harga">Rp. 300.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/sokdepan.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Sok Depan</h5>
         <label class="card-text harga">Rp. 700.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/spion.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Spion</h5>
         <label class="card-text harga">Rp. 70.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/tabungmasterrem.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Tabung Rem</h5>
         <label class="card-text harga">Rp. 100.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
        <div class="card border-dark">
       <img src="image/velg.jpg" class="card-img-top" alt="...">
         <div class="card-body">
             <h5 class="card-title font-weight-bold">Velg</h5>
             <label class="card-text harga">Rp. 85.000</label><br>
         </div>
        </div>
      </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/helmgvktigasv.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Helm GVK 3 SV</h5>
         <label class="card-text harga">Rp. 600.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/gear set aspiran.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Gear Set Aspiran</h5>
         <label class="card-text harga">Rp. 600.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/sepur gear.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Sepur Gear</h5>
         <label class="card-text harga">Rp. 140.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/ban motot.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Ban Motor</h5>
         <label class="card-text harga">Rp. 300.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/knalpot racing.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Knalpot Racing</h5>
         <label class="card-text harga">Rp. 600.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/baut.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Baut</h5>
         <label class="card-text harga">Rp. 10.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/setir.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Setir</h5>
         <label class="card-text harga">Rp. 200.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/stang variasi.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Stang Variasi</h5>
         <label class="card-text harga">Rp. 100.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/handel motor.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Handel Motor</h5>
         <label class="card-text harga">Rp. 70.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/handlebar balance.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Handlebar</h5>
         <label class="card-text harga">Rp. 70.000</label><br>
     </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/pegangan grip.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Pegangan Grip</h5>
         <label class="card-text harga">Rp. 70.000</label><br>
     </div>
    </div>
  </div>
</div>      
   
      <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp80k3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpgZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src=""></script>
</body>
</html>