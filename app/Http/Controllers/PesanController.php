<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\saya;

class PesanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dtpesan = saya::all();
        return view('pesan1', compact('dtpesan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pesan');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd ($request->all());
        saya::create([
            'nama_barang' => $request->nama,
            'harga_barang' => $request->harga,
            'jumlah_pesanan' => $request->jumlah,
            'tanggal' => $request->tanggal,
        ]);
        return redirect('pesan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = saya::find($id);
        return view('edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = saya::find($id);
        $save = $data->update([
            'nama_barang' => $request->nama,
            'harga_barang' => $request->harga,
            'jumlah_pesanan' => $request->jumlah,
            'tanggal' => $request->tanggal,
        ]);
        if($save){
            $dtpesan = saya::all();
            return redirect()->route('pesan', compact('dtpesan'));
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pes = saya::findorfail($id);
        $pes->delete();
        return back();
    }
}
