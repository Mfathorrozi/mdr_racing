<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="styles.css">
    <title>MDR RACING</title>
  </head>
  <body>

  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">TOKO AKSESORIS MDR RACING</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
      <div class="navbar-nav">
        <a class="nav-link active" aria-current="page" href="#">Home</a>
        <a class="nav-link" href="">DAFTAR AKSESORIS</a>
      </div>
    </div>
  </div>
</nav>
  <h2>TOKO AKSESORIS MDR RACING</h2>
    <section>
        <div class="content">
            <div class="card card-info card-outline">
                <div class="card-header">
                    <h3 class="alert alert-primary text-center mt-3">Edit Pesanan Anda</h3>
                </div>
                <div class="card-body">
                    <form action="{{ route('edit',$data->id)}}" method="POST">
                        @csrf
                        @method('put')
                        <div class="from-group mt-2">
                            <input type="text" id=nama name="nama" class="form-control" placeholder="Nama barang" >
                        </div>
                        <div class="from-group mt-2">
                            <input type="text" id=harga name="harga" class="form-control" placeholder="Harga Barang" >
                        </div>
                        <div class="from-group mt-2">
                            <input type="text" id=jumlah name="jumlah" class="form-control" placeholder="Jumlah pesanan" >
                        </div>
                        <div class="from-group mt-2">
                            <input type="date" id=tanggal name="tanggal" class="form-control" >
                        </div>
                        <div class="from-group mt-2">
                            <button type="submit" class="btn btn-primary">ubah</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

   
  </body>
</html>