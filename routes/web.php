<?php

use App\Http\Controllers\PesanController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('saya', function () {
    return view('daftaraksesoris');
});
Route::get('/pesan', [PesanController::class, 'index'])->name('pesan') ;    
Route::get('/tambahpesan', [PesanController::class, 'create'])->name('tambahpesan') ;    
Route::get('/simpan', [PesanController::class, 'store'])->name('simpan') ;    
Route::get('/edit/{id}', [PesanController::class, 'edit'])->name('edit') ;    
Route::put('/edit/{id}', [PesanController::class, 'update'])->name('edit') ;    
Route::get('/hapus/{id}', [PesanController::class, 'destroy'])->name('hapus') ; 