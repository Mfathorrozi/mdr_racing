<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class saya extends Model
{
    protected $table ="sayas";
    protected $primariKey ="id";
    protected $fillable = [
        'id','nama_barang','harga_barang','jumlah_pesanan','tanggal'
    ];
}
